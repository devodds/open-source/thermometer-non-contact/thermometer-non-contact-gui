#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include "mlx90614.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label_3->hide();
    ui->label_4->hide();
    timer = new QTimer(this);
    //timer->setSingleShot(false); //Setting this to true makes the timer run only once
    connect(timer, SIGNAL(timeout()), this, SLOT(UpdateTemperature()));
    timer->start(1000); // Interval 0 means to refresh as fast as possible
    timer->start(); //Call start() AFTER connect
    //Do not call start(0) since this will change interval
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::UpdateTemperature() {
    static int i = 0;
    static bool muestra = true;
    static double temp_old;
    mlx90614 *mlx;
    mlx = new mlx90614(2, 0x5a);
    double temp = mlx->readFullSensorState();
    if (muestra || temp > temp_old) {
        temp_old = temp;
        if(temp <= 32) {
            ui->label_2->show();
            ui->label_3->hide();
            ui->label_4->hide();
            ui->centralwidget->setStyleSheet("background-color:white;");
        } else if(temp > 32 && temp <= 36) {
            muestra = false;
            ui->label_2->hide();
            ui->label_3->show();
            ui->label_4->show();
            ui->centralwidget->setStyleSheet("background-color:green;");
            ui->label_4->setText(QString::number(temp, 'f', 1).append(QString("°C")));
        } else if(temp > 36 && temp <= 37.5) {
            muestra = false;
            ui->centralwidget->setStyleSheet("background-color:orange;");
            ui->label_2->hide();
            ui->label_3->show();
            ui->label_4->show();
            ui->label_4->setText(QString::number(temp, 'f', 1).append(QString("°C")));
        } else {
            muestra = false;
            ui->label_2->hide();
            ui->label_3->show();
            ui->label_4->show();
            ui->centralwidget->setStyleSheet("background-color:red;");
            ui->label_4->setText(QString::number(temp, 'f', 1).append(QString("°C")));
        }
    } else {
        if(i > 5) {
            muestra = true;
            temp_old = 0;
            i = 0;
        } else {
            i++;
        }
    }

}
