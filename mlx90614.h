#ifndef MLX90614_H
#define MLX90614_H

#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

#define BMA180_I2C_BUFFER 0x80

class mlx90614
{
private:
    int I2CBus, I2CAddress;
    char dataBuffer[BMA180_I2C_BUFFER];

public:
    mlx90614(int bus, int address);

    double  readFullSensorState();


    static inline __s32 i2c_smbus_access(int file, char read_write, __u8 command,
                                         int size, union i2c_smbus_data *data)
    {
        struct i2c_smbus_ioctl_data args;

        args.read_write = read_write;
        args.command = command;
        args.size = size;
        args.data = data;
        return ioctl(file,I2C_SMBUS,&args);
    }

    static inline __s32 i2c_smbus_read_word_data(int file, __u8 command)
    {
        union i2c_smbus_data data;
        if (i2c_smbus_access(file,I2C_SMBUS_READ,command,
                             I2C_SMBUS_WORD_DATA,&data))
            return -1;
        else
            return 0x0FFFF & data.word;
    }

    static inline __s32 i2c_smbus_read_byte(int file)
    {
        union i2c_smbus_data data;
        if (i2c_smbus_access(file,I2C_SMBUS_READ,0,I2C_SMBUS_BYTE,&data))
            return -1;
        else
            return 0x0FF & data.byte;
    }
    static inline __s32 i2c_smbus_read_byte_data(int file, __u8 command)
    {
        union i2c_smbus_data data;
        if (i2c_smbus_access(file,I2C_SMBUS_READ,command,
                             I2C_SMBUS_BYTE_DATA,&data))
            return -1;
        else
            return 0x0FF & data.byte;
    }
};

#endif // MLX90614_H
