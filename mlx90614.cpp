#include "mlx90614.h"
#include <unistd.h>
#include <fcntl.h>
#include <iostream>

#define MAX_BUS 64

using namespace std;
mlx90614::mlx90614(int bus, int address) {
    I2CBus = bus;
    I2CAddress = address;
    readFullSensorState();
}

double mlx90614::readFullSensorState() {
    cout << "Starting MLX90614 I2C sensor read" << endl;
    char namebuf[MAX_BUS];
    snprintf(namebuf, sizeof(namebuf), "/dev/i2c-%d", I2CBus);
    int file;
    if ((file = open(namebuf, O_RDWR)) < 0) {
        cout << "Failed to open MLX90614 Sensor on " << namebuf << " I2C Bus" << endl;
        return(1);
    }
//    if (ioctl(file, I2C_SLAVE, I2CAddress) < 0) {
//        cout << "I2C_SLAVE address " << I2CAddress << " failed..." << endl;
//        return(2);
//    }
    const char REGISTER_ID = 0x07;
    int data = i2c_smbus_read_word_data(file, REGISTER_ID);
    double temp = data * 0.02 - 273.15;
    return temp;
}
